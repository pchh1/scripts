@echo off

:: Prompt to Run as administrator
Set "Variable=0" & if exist "%temp%\getadmin.vbs" del "%temp%\getadmin.vbs"
fsutil dirty query %systemdrive%  >nul 2>&1 && goto :(Privileges_got)
If "%1"=="%Variable%" (echo. &echo. Please right-click on the file and select &echo. "Run as administrator". &echo. Press any key to exit. &pause>nul 2>&1& exit)
cmd /u /c echo Set UAC = CreateObject^("Shell.Application"^) : UAC.ShellExecute "%~0", "%Variable%", "", "runas", 1 > "%temp%\getadmin.vbs"&cscript //nologo "%temp%\getadmin.vbs" & exit
:(Privileges_got)

echo Running sfc
sfc /scannow

echo Running DISM
dism /Online /Cleanup-Image /CheckHealth
dism /Online /Cleanup-Image /ScanHealth
dism /Online /Cleanup-Image /RestoreHealth

echo Running chkdsk
chkdsk /r C:

:: Restart computer
cls
echo It is required to restart the computer to finish resetting Windows Update.
echo.
echo Please save and close anything open now, before the computer is restarted.
echo.
pause
echo.
echo.
echo.
echo *** Restart computer now. ***
echo.
pause
shutdown /r /f /t 0
