@echo off

:: Prompt to Run as administrator
Set "Variable=0" & if exist "%temp%\getadmin.vbs" del "%temp%\getadmin.vbs"
fsutil dirty query %systemdrive%  >nul 2>&1 && goto :(Privileges_got)
If "%1"=="%Variable%" (echo. &echo. Please right-click on the file and select &echo. "Run as administrator". &echo. Press any key to exit. &pause>nul 2>&1& exit)
cmd /u /c echo Set UAC = CreateObject^("Shell.Application"^) : UAC.ShellExecute "%~0", "%Variable%", "", "runas", 1 > "%temp%\getadmin.vbs"&cscript //nologo "%temp%\getadmin.vbs" & exit
:(Privileges_got)

:: KILL EXPLORER SHELL
echo Stopping explorer.
taskkill /f /im explorer.exe
timeout /t 5 >nul

:: RESET STARTUP
echo Resetting user startup directory.
if not exist "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup" mkdir "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup"
reg add "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v "Startup" /t REG_SZ /d "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup" /f /reg:64

:: RESET COMMON STARTUP
echo Resetting system startup directory.
if not exist "%ProgramData%\Microsoft\Windows\Start Menu\Programs\Startup" mkdir "%ProgramData%\Microsoft\Windows\Start Menu\Programs\Startup"
reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\User Shell Folders" /v "Common Startup" /t REG_SZ /d "%ProgramData%\Microsoft\Windows\Start Menu\Programs\Startup"

:: RELOAD EXPLORER SHELL
echo Starting explorer.
timeout /t 5 >nul
start "" explorer.exe

:: Restart computer
cls
echo It is required to restart the computer to finish resetting the Startup paths.
echo.
echo Please save and close anything open now, before the computer is restarted.
echo.
pause
echo.
echo.
echo.
echo *** Restart computer now. ***
echo.
pause
shutdown /r /f /t 0
